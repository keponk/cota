package cota;

import javax.media.opengl.GL2;
import javafx.geometry.Point3D;

/**
 * Implements an Octagon in OpenGL
 *
 */
public class CotaOctagon extends CotaFigure {

	private Point3D centralPoint;
	private float radius;
	private float[] colorRGB;
	private float canvasPosX;
	private float canvasPosY;
	private float canvasPosZ;

	public CotaOctagon(Point3D position, float radius) {
		this.radius = radius;
		setPosition(position);
		colorRGB = CotaFigure.FOREST_GREEN;
	}

	@Override
	public void draw(GL2 gl) {
		double angle = 2 * 3.14 / 8;
		gl.glBegin(GL2.GL_POLYGON);
		for (int i = 0; i < 8; ++i) {
			gl.glVertex3f(canvasPosX + (float) (radius * Math.cos(angle * i)),
					canvasPosY + (float) (radius * Math.sin(angle * i)), canvasPosZ);
		}
		gl.glEnd();
	}

	@Override
	public Point3D getPosition() {
		return centralPoint;
	}

	@Override
	public void setPosition(Point3D position) {
		centralPoint = position;
		canvasPosX = (float) centralPoint.getX();
		canvasPosY = (float) centralPoint.getY();
		canvasPosZ = (float) -centralPoint.getZ();
	}

	@Override
	public float getRadius() {
		return radius;
	}

	@Override
	public float[] getPositionOnCanvas() {
		float[] canvasPosition = { canvasPosX, canvasPosY, canvasPosZ };
		return canvasPosition;
	}

	@Override
	public void translateFigure(GL2 gl, Point3D position, float offsetX, float offsetY, float offsetZ) {
		gl.glTranslatef((float) position.getX() + offsetX, (float) position.getY() + offsetY,
				(float) position.getZ() + offsetZ);
	}

	@Override
	public void setColor(float red, float green, float blue) {
		colorRGB[0] = red;
		colorRGB[1] = green;
		colorRGB[2] = blue;
	}

	@Override
	public float[] getColor() {
		return colorRGB;
	}

	@Override
	public int getShape() {
		return CotaFigure.OCTAGON;
	}

}
