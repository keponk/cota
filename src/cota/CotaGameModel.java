package cota;

import java.util.ArrayList;
import java.util.Random;
import javafx.geometry.Point3D;

/**
 * Model class
 * 
 * Stores data about the hands, the objects to interact with and the targets
 *
 */
public class CotaGameModel {

	private int currentLevel;
	private CotaHand rightHand;
	private CotaHand leftHand;
	private Point3D startMenu; // the starting position of the bottom menu
	private float menuSideSize; // the size of the squares in the bottom menu
	private float menuOffset; // the distance between two items in the bottom menu
	private float menuDepth; // the z coordinate of the items in the bottom menu
	private boolean[] targetsAchieved;
	private int noTargetsAchieved;
	private int noMenuShapes; // number of shapes in the menu
	private ArrayList<CotaFigure> shapesMenu;
	private ArrayList<CotaFigure> shapesMenuBackground;
	private ArrayList<CotaFigure> targetItems;
	private ArrayList<CotaFigure> removalObjects; // the trash bin objects

	public CotaGameModel() {
		noMenuShapes = 5;
		shapesMenu = new ArrayList<CotaFigure>();
		shapesMenuBackground = new ArrayList<CotaFigure>();
		targetItems = new ArrayList<CotaFigure>();
		removalObjects = new ArrayList<CotaFigure>();
		setRemovalObjects();

		currentLevel = 1;
		noTargetsAchieved = 0;
		targetsAchieved = new boolean[currentLevel];
		startMenu = new Point3D(-1.0, -0.45f, 0.0);
		menuDepth = 2f;
		menuSideSize = 0.3f;
		menuOffset = 0.35f;

		loadShapesMenuBackground();
		loadShapesMenu();
		loadTargets(currentLevel);

		rightHand = new CotaHand();
		leftHand = new CotaHand();
	}

	public int getNoTargetsAchieved() {
		return noTargetsAchieved;
	}

	public void setRemovalObjects() {
		removalObjects.add(new CotaOctagon(new Point3D(-1.1, 0.4, 2), 0.15f));
		removalObjects.add(new CotaOctagon(new Point3D(1.1, 0.4, 2), 0.15f));
	}

	/**
	 * Pass to the next level
	 */
	public void nextLevel() {
		++currentLevel;
		noTargetsAchieved = 0;
		targetsAchieved = new boolean[currentLevel];
		targetItems = new ArrayList<CotaFigure>();
		loadTargets(currentLevel);
	}

	public int getLevel() {
		return this.currentLevel;
	}

	/**
	 * Load #level number of randomly positioned targets on screen
	 * @param level
	 */
	private void loadTargets(int level) {
		Random rand = new Random();

		for (int i = 0; i < level; i++) {
			int n = rand.nextInt(CotaFigure.SHAPES_AVAILABLE) + 1;
			Point3D pos = generateRandomPosition();
			CotaFigure target = null;
			switch (n) {
			case 1:
				target = new CotaCircle(pos, 0.25f);
				break;
			case 2:
				target = new CotaTriangle(pos, 0.1f);
				break;
			case 3:
				target = new CotaRectangle(pos, 0.25f, 0.25f);
				break;
			case 4:
				target = new CotaHexagon(pos, 0.15f);
				break;
			case 5:
				target = new CotaOctagon(pos, 0.15f);
				break;
			}
			targetItems.add(target);
			targetsAchieved[i] = false;
		}
	}

	/**
	 * @param min
	 * @param max
	 * @return random number in the range (min, max)
	 */
	private float randomFloat(float min, float max) {
		Random rand = new Random();

		return rand.nextFloat() * (max - min) + min;
	}

	/**
	 * Generate random position on screen that does not overlap
	 * with other target items, bottom menu or trash bin objects
	 * @return
	 */
	private Point3D generateRandomPosition() {
		float x = randomFloat((float) startMenu.getY() + 0.3f, 0.7f);
		float y = randomFloat((float) startMenu.getY() + 0.3f, 0.7f);
		float z = menuDepth;
		Point3D pos = new Point3D(x, y, z);
		boolean goodPos = true;

		for (CotaFigure target : targetItems) {
			if (checkCollision(target.getPosition(), target.getRadius(), pos, 0.2f)) {
				goodPos = false;
			}
		}

		for (CotaFigure shape : shapesMenu) {
			if (checkCollision(shape.getPosition(), shape.getRadius(), pos, 0.2f)) {
				goodPos = false;
			}
		}

		for (CotaFigure obj : removalObjects) {
			if (checkCollision(obj.getPosition(), obj.getRadius(), pos, 0.2f)) {
				goodPos = false;
			}
		}

		if (goodPos) {
			return pos;
		} else {
			return generateRandomPosition();
		}
	}

	/**
	 * Check collision between two objects
	 * @param obj1
	 * @param radius1
	 * @param obj2
	 * @param radius2
	 * @return collision status
	 */
	private boolean checkCollision(Point3D obj1, float radius1, Point3D obj2, float radius2) {
		double radii = 0.25;
		double p1x = obj1.getX();
		double p1y = obj1.getY();
		double p1z = obj1.getZ();
		double p2x = obj2.getX();
		double p2y = obj2.getY();
		double p2z = obj2.getZ();
		double distance = Math
				.sqrt(((p1x - p2x) * (p1x - p2x)) + ((p1y - p2y) * (p1y - p2y)) + ((p1z - p2z) * (p1z - p2z)));
		return radii >= distance;
	}

	/**
	 * Create the background squares of the shapes in the bottom menu
	 */
	private void loadShapesMenuBackground() {
		for (int i = 0; i < noMenuShapes; i++) {
			CotaRectangle rect = new CotaRectangle(
					new Point3D(startMenu.getX() + i * (menuSideSize / 2 + menuOffset), startMenu.getY(), menuDepth),
					menuSideSize, menuSideSize);
			shapesMenuBackground.add(rect);
		}
	}

	/**
	 * Create the shapes in the bottom menu
	 */
	private void loadShapesMenu() {
		CotaTriangle shape1 = new CotaTriangle(new Point3D(startMenu.getX(), startMenu.getY() - 0.03f, menuDepth),
				0.07f);
		CotaRectangle shape2 = new CotaRectangle(
				new Point3D(startMenu.getX() + (menuSideSize / 2 + menuOffset), startMenu.getY(), menuDepth), 0.2f,
				0.2f);
		CotaCircle shape3 = new CotaCircle(
				new Point3D(startMenu.getX() + 2 * (menuSideSize / 2 + menuOffset), startMenu.getY(), menuDepth), 0.2f);
		CotaHexagon shape4 = new CotaHexagon(
				new Point3D(startMenu.getX() + 3 * (menuSideSize / 2 + menuOffset), startMenu.getY(), menuDepth), 0.1f);
		CotaOctagon shape5 = new CotaOctagon(
				new Point3D(startMenu.getX() + 4 * (menuSideSize / 2 + menuOffset), startMenu.getY(), menuDepth), 0.1f);

		shapesMenu.add(shape1);
		shapesMenu.add(shape2);
		shapesMenu.add(shape3);
		shapesMenu.add(shape4);
		shapesMenu.add(shape5);
	}
	
	public CotaHand getRightHand() {
		return rightHand;
	}
	
	public void updateRightHand(double[] rightHandVector) {
		if (rightHand == null) {
			rightHand = new CotaHand(new Point3D(rightHandVector[0], rightHandVector[1], rightHandVector[2]));
		} else {
			rightHand.setPosition(new Point3D(rightHandVector[0], rightHandVector[1], rightHandVector[2]));
		}
	}
	
	public CotaHand getLeftHand() {
		return leftHand;
	}
	
	public void updateLeftHand(double[] leftHandVector) {
		if (leftHand == null) {
			leftHand = new CotaHand(new Point3D(leftHandVector[0], leftHandVector[1], leftHandVector[2]));
		} else {
			leftHand.setPosition(new Point3D(leftHandVector[0], leftHandVector[1], leftHandVector[2]));
		}
	}
	
	/**
	 * Reset the game to the original parameters from the constructor
	 */
	public void resetGame() {
		noMenuShapes = 5;
		shapesMenu = new ArrayList<CotaFigure>();
		shapesMenuBackground = new ArrayList<CotaFigure>();
		targetItems = new ArrayList<CotaFigure>();
		removalObjects = new ArrayList<CotaFigure>();
		setRemovalObjects();

		currentLevel = 1;
		noTargetsAchieved = 0;
		targetsAchieved = new boolean[currentLevel];
		startMenu = new Point3D(-1.0, -0.45f, 0.0);
		menuDepth = 2f;
		menuSideSize = 0.3f;
		menuOffset = 0.35f;

		loadShapesMenuBackground();
		loadShapesMenu();
		loadTargets(currentLevel);

		rightHand = new CotaHand();
		leftHand = new CotaHand();
	}


	public ArrayList<CotaFigure> getShapesMenu() {
		return shapesMenu;
	}

	public void setShapesMenu(ArrayList<CotaFigure> shapesMenu) {
		this.shapesMenu = shapesMenu;
	}

	public ArrayList<CotaFigure> getShapesMenuBackground() {
		return shapesMenuBackground;
	}

	public void setShapesMenuBackground(ArrayList<CotaFigure> shapesMenuBackground) {
		this.shapesMenuBackground = shapesMenuBackground;
	}

	public ArrayList<CotaFigure> getTargetItems() {
		return targetItems;
	}

	public boolean[] getTargetsAchieved() {
		return targetsAchieved;
	}

	public boolean isTargetAchieved(int index) {
		return targetsAchieved[index];
	}

	public void setTargetsAchieved(int index, boolean status) {
		targetsAchieved[index] = status;
		++noTargetsAchieved;
	}

	public ArrayList<CotaFigure> getRemovalObjects() {
		return removalObjects;
	}
}
